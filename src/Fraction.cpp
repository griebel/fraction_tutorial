/**
 * \brief ‫Implements methods of the fraction class
 *
 * \author Carlos Villarraga
 *
 */

#include "Fraction.hpp" 

Fraction::Fraction(int n, int d) throw (DivisionByZeroException) {

}


Fraction& Fraction::operator *=(const Fraction& rhs) {
	return *this;
}

int Fraction::num() const {
	return 0;
}

int Fraction::den() const {
	return 0;
}

Fraction operator *(const Fraction& lhs, const Fraction& rhs) {
	return 0;
}

bool Fraction::operator ==(const Fraction& rhs) const {
	return false;
}

bool Fraction::operator !=(const Fraction& rhs) const {
	return false;
}

ostream& operator <<(ostream& out, const Fraction& rhs) {
	return out << "";
}

